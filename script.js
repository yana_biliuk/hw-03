// task 1

let years = prompt("Скільки вам років");

if (isNaN(years) || years === null || years === "") {
  alert("Не вірне значення ");
} else {
  if (years < 12) {
    alert("Ти ще дитина");
  } else if (years >= 12 && years < 18) {
    alert("Ти підліток");
  } else if (years >= 18) {
    alert("Ти дорослий");
  }
}

// task 2
let month = prompt(" Введіть місяць року")

let monthDay = month.toLowerCase();

switch (monthDay) {
  case "січень":
  case "березень":
  case "травень":
  case "липень":
  case "серпень":
  case "жовтень":
  case "грудень":
    console.log("31 днів ");
    break;
  case "лютий":
    console.log("28 днів");
    break;
  case "квітень":
  case "червень":
  case "вересень":
  case "листопад":
    console.log("30 днів");
    break;
  default:
    console.log("Не вірно ");
}
